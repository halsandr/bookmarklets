window.brandOverride = (token) => {
    const checkOverride = () => {
        return $('#brand-select option:selected').val();
    }
    const setOverride = () => {
        const brand = checkOverride();
        let url;
        let data;
        if (!brand) {
            url = '/systemsupport/clearoverride';

            if (typeof MI !== 'undefined') {
                data = { 'name': 'domain' };
            } else {
                data = { 'name': 'brand' };
            }
        } else {
            url = '/systemsupport/setoverride';

            if (typeof MI !== 'undefined') {
                data = { 'name': 'domain', 'value': brand };
            } else {
                data = { 'name': 'brand', 'value': brand };
            }
        }

        $.get(url, data, () => location.reload());
    };
    $.post('/systemsupport/brandlist', {
        "token": token
    }, result => {
        const brandDialog = $("<div>")
        brandDialog.attr('id', 'brand-dialog')
        brandDialog.css({ 'position': 'fixed', 'top': '50%', 'left': '50%', 'transform': 'translateX(-50%)', 'padding': '30px', 'background-color': 'white', 'border-radius': '10px', 'box-shadow': '0px 0px 20px 0px', 'padding': '20px', 'width': '90%', 'max-width': '300px', 'z-index': '9999' });

        const closeButton = $('<span>');
        closeButton.attr('id', 'closeButton');
        closeButton.addClass(["fa", "fa-lg", "fa-times"]);
        closeButton.css({ "position": "absolute", "right": "20px", "cursor": "pointer" });
        closeButton.click(() => brandDialog.remove());

        const title = $('<h1>');
        title.text("Select your brand")

        const brandSelect = $('<select>');
        brandSelect.attr('id', 'brand-select');

        const defaultOption = $('<option>');
        defaultOption.text('None');
        defaultOption.val('');
        brandSelect.append(defaultOption);

        for (let i = 0; i < result.data.length; i++) {
            const brand = result.data[i];
            const option = $('<option>');
            option.text(brand);
            option.val(brand);
            brandSelect.append(option);
        }

        const overrideButton = $('<button>');
        overrideButton.text('apply');
        overrideButton.attr('id', 'overrideButton');
        overrideButton.click(setOverride);
        if (typeof MI !== 'undefined') {
            overrideButton.addClass(["cta-button", "dark", "text", "m-1"]);
        } else {
            overrideButton.addClass(["btn", "btn-primary", "m-1"]);
        }

        brandDialog.append(closeButton);
        brandDialog.append(title);
        brandDialog.append(brandSelect);
        brandDialog.append(overrideButton);

        if ($('#brand-dialog').length < 1) {
            $('body').append(brandDialog);
        }
    });
}
